package com.ogi.layarkacamobile.fragment;


import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.ogi.layarkacamobile.MainActivity;
import com.ogi.layarkacamobile.R;
import com.ogi.layarkacamobile.adapter.MoviesAdapter;
import com.ogi.layarkacamobile.api.Client;
import com.ogi.layarkacamobile.api.Service;
import com.ogi.layarkacamobile.model.movie.MovieData;
import com.ogi.layarkacamobile.model.movie.MoviesResponse;
import com.ogi.layarkacamobile.util.PaginationScrollListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class MoviesFragment extends Fragment {
    private static final String TAG = "MoviesFragment";

    RecyclerView rvMoviesList;
    ProgressBar pbMovies;
    MoviesAdapter moviesAdapter;
    LinearLayoutManager linearLayoutManager;
    List<MovieData> results;

    private static final int PAGE_START = 1;
    private boolean isLoading = false;
    private boolean isLastPage = false;

    private int TOTAL_PAGES = 10;
    private int currentPage = PAGE_START;
    private static final String MOVIE_STAT = "movie_stat";
    private static final String CURRENT_PAGE = "current_page";

    private Service movieService;

    public MoviesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout = LayoutInflater.from(getContext()).inflate(R.layout.fragment_movies, container, false);
        rvMoviesList = layout.findViewById(R.id.rv_movies_id);
        pbMovies = layout.findViewById(R.id.pb_movies_id);
        moviesAdapter = new MoviesAdapter(getContext());

        linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rvMoviesList.setLayoutManager(linearLayoutManager);

        rvMoviesList.setItemAnimator(new DefaultItemAnimator());
        rvMoviesList.setAdapter(moviesAdapter);

        rvMoviesList.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;

                // mocking network delay for API  call
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loadNextPage();
                    }
                }, 1000);
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        // init service and load data
        movieService = Client.getClient().create(Service.class);

        if(savedInstanceState != null){
            results = savedInstanceState.getParcelableArrayList(MOVIE_STAT);
            currentPage = savedInstanceState.getInt(CURRENT_PAGE, 1);
            pbMovies.setVisibility(View.GONE);
            moviesAdapter.addAll(results);
        } else
            loadFirstPage();

        return layout;
    }

    private void loadFirstPage() {
        Log.d(TAG, "LoadFirstPage");

        callPopularMovieApi()
                .enqueue(new Callback<MoviesResponse>() {
                    @Override
                    public void onResponse(Call<MoviesResponse> call, Response<MoviesResponse> response) {
                        // Got data send into adapter
                        results = fetchResults(response);
                        pbMovies.setVisibility(View.GONE);
                        moviesAdapter.addAll(results);

                        if (currentPage <= TOTAL_PAGES) moviesAdapter.addLoadingFooter();
                        else isLastPage = true;
                    }

                    @Override
                    public void onFailure(Call<MoviesResponse> call, Throwable t) {
                        t.printStackTrace();
                    }
                });

    }

    private List<MovieData> fetchResults(Response<MoviesResponse> response) {
        MoviesResponse popularMovies = response.body();
        return popularMovies.getResults();
    }

    private void loadNextPage() {
        Log.d(TAG, "loadNextPage "+ currentPage);
        callPopularMovieApi()
                .enqueue(new Callback<MoviesResponse>() {
                    @Override
                    public void onResponse(Call<MoviesResponse> call, Response<MoviesResponse> response) {
                        moviesAdapter.removeLoadingFooter();
                        isLoading = false;

                        List<MovieData> nextResults = fetchResults(response);
                        moviesAdapter.addAll(nextResults);
                        results.addAll(nextResults);

                        if (currentPage != TOTAL_PAGES)moviesAdapter.addLoadingFooter();
                        else isLastPage = true;
                    }

                    @Override
                    public void onFailure(Call<MoviesResponse> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
    }

    private Call<MoviesResponse> callPopularMovieApi() {
        return  movieService.getPopularMovies("f6602517b834e9ce06a48548f949e397", currentPage);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(MOVIE_STAT, new ArrayList<>(results));
        outState.putInt(CURRENT_PAGE, currentPage);
    }
}
