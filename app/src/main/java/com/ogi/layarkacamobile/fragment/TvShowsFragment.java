package com.ogi.layarkacamobile.fragment;


import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.ogi.layarkacamobile.R;
import com.ogi.layarkacamobile.adapter.TvShowsAdapter;
import com.ogi.layarkacamobile.api.Client;
import com.ogi.layarkacamobile.api.Service;
import com.ogi.layarkacamobile.model.tv.TvShowData;
import com.ogi.layarkacamobile.model.tv.TvShowsResponse;
import com.ogi.layarkacamobile.util.PaginationScrollListener;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class TvShowsFragment extends Fragment {
    private static final String TAG = "TvShowsFragment";

    RecyclerView rvTvShowsList;
    ProgressBar pbTvShows;
    TvShowsAdapter tvShowsAdapter;
    LinearLayoutManager linearLayoutManager;

    private static final int PAGE_START = 1;
    private boolean isLoading = false;
    private boolean isLastPage = false;

    private int TOTAL_PAGES = 10;
    private int currentPage = PAGE_START;

    private Service tvShowService;

    public TvShowsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout = LayoutInflater.from(getContext()).inflate(R.layout.fragment_tv_shows, container, false);
        rvTvShowsList = layout.findViewById(R.id.rv_tv_shows_id);
        pbTvShows = layout.findViewById(R.id.pb_tv_shows_id);

        tvShowsAdapter = new TvShowsAdapter(getContext());

        linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);

        rvTvShowsList.setLayoutManager(linearLayoutManager);

        rvTvShowsList.setLayoutManager(linearLayoutManager);

        rvTvShowsList.setItemAnimator(new DefaultItemAnimator());
        rvTvShowsList.setAdapter(tvShowsAdapter);

        rvTvShowsList.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;

                // mocking network delay for API  call
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loadNextPage();
                    }
                }, 1000);
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        // init service and load data
        tvShowService = Client.getClient().create(Service.class);

        loadFirstPage();

        return layout;
    }

    private void loadFirstPage() {
        Log.d(TAG, "LoadFirstPage");

        callPopularTvShowApi()
                .enqueue(new Callback<TvShowsResponse>() {
                    @Override
                    public void onResponse(Call<TvShowsResponse> call, Response<TvShowsResponse> response) {
                        // Got data send into adapter
                        List<TvShowData> results = fetchResults(response);
                        pbTvShows.setVisibility(View.GONE);
                        tvShowsAdapter.addAll(results);

                        if (currentPage <= TOTAL_PAGES) tvShowsAdapter.addLoadingFooter();
                        else isLastPage = true;
                    }

                    @Override
                    public void onFailure(Call<TvShowsResponse> call, Throwable t) {
                        t.printStackTrace();
                    }
                });

    }

    private List<TvShowData> fetchResults(Response<TvShowsResponse> response) {
        TvShowsResponse popularMovies = response.body();
        return popularMovies.getResults();
    }

    private void loadNextPage() {
        Log.d(TAG, "loadNextPage "+ currentPage);
        callPopularTvShowApi()
                .enqueue(new Callback<TvShowsResponse>() {
                    @Override
                    public void onResponse(Call<TvShowsResponse> call, Response<TvShowsResponse> response) {
                        tvShowsAdapter.removeLoadingFooter();
                        isLoading = false;

                        List<TvShowData> results = fetchResults(response);
                        tvShowsAdapter.addAll(results);

                        if (currentPage != TOTAL_PAGES)tvShowsAdapter.addLoadingFooter();
                        else isLastPage = true;
                    }

                    @Override
                    public void onFailure(Call<TvShowsResponse> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
    }

    private Call<TvShowsResponse> callPopularTvShowApi() {
        return tvShowService.getPopularTvShows("f6602517b834e9ce06a48548f949e397", currentPage);
    }

}
