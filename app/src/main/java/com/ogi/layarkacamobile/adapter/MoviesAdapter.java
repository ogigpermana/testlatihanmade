package com.ogi.layarkacamobile.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.ogi.layarkacamobile.DetailMovieActivity;
import com.ogi.layarkacamobile.R;
import com.ogi.layarkacamobile.model.movie.MovieData;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class MoviesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int ITEM = 0;
    private static final int LOADING = 1;

    private Context context;
    private List<MovieData> movieResults;

    private boolean isLoadingAdded = false;

    public MoviesAdapter(Context context) {
        this.context = context;
        movieResults = new ArrayList<>();
    }

    public List<MovieData> getMovies() {
        return movieResults;
    }

    public void setMovies(List<MovieData> movieResults) {
        this.movieResults = movieResults;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM:
                viewHolder = getViewHolder(parent, inflater);
                break;
            case LOADING:
                View vProgress = inflater.inflate(R.layout.item_progressbar, parent, false);
                viewHolder = new LoadingViewHolder(vProgress);
                break;
        }
        return viewHolder;
    }

    @NonNull
    private RecyclerView.ViewHolder getViewHolder(ViewGroup parent, LayoutInflater inflater) {
        RecyclerView.ViewHolder viewHolder;
        View vItemMovies = inflater.inflate(R.layout.item_movies_list, parent, false);
        viewHolder = new MovieViewHolder(vItemMovies);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        String IMAGE_BASE_URL = "https://image.tmdb.org/t/p/w500";
        MovieData result = movieResults.get(position); // movie
        switch (getItemViewType(position)) {
            case ITEM:
            final MovieViewHolder movieViewHolder = (MovieViewHolder) holder;
            movieViewHolder.tvTitle.setText(result.getTitle());
            movieViewHolder.tvOverview.setText(result.getOverview());
            movieViewHolder.tvReleaseDate.setText(result.getReleaseDate().split("-")[0]);
            movieViewHolder.tvRating.setText(String.valueOf(result.getVoteAverage()));
            movieViewHolder.rbRating.setRating((float) (result.getVoteAverage() / 2));
                Picasso.get()
                        .load(IMAGE_BASE_URL+result.getPosterPath())
                        .placeholder(R.drawable.default_poster_path)
                        .error(R.drawable.broken_image)
                        .into(movieViewHolder.ivPosterPath);
                Log.d("ImageUrl", IMAGE_BASE_URL+result.getPosterPath()+" something went wrong");
                break;
            case LOADING:
                break;
        }
    }

    @Override
    public int getItemCount() {
        return movieResults == null ? 0 : movieResults.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == movieResults.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }

    // Helpers
    private void add(MovieData rslt) {
        movieResults.add(rslt);
        notifyItemInserted(movieResults.size() - 1);
    }

    public void addAll(List<MovieData> movieDataList) {
        for (MovieData result : movieDataList ) {
            add(result);
        }
    }

    private void remove(MovieData rmData) {
        int position = movieResults.indexOf(rmData);
        if (position > -1) {
            movieResults.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new MovieData());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = movieResults.size() - 1;
        MovieData result = getItem(position);

        if (result != null) {
            movieResults.remove(position);
            notifyItemRemoved(position);
        }
    }

    private MovieData getItem(int position) {
        return movieResults.get(position);
    }

    private class MovieViewHolder extends RecyclerView.ViewHolder{
        private TextView tvTitle, tvRating, tvOverview, tvReleaseDate;
        private ImageView ivPosterPath;
        private RatingBar rbRating;
        private MovieViewHolder(@NonNull final View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvRating = itemView.findViewById(R.id.tv_vote_average);
            tvReleaseDate = itemView.findViewById(R.id.tv_release_date);
            ivPosterPath = itemView.findViewById(R.id.iv_poster_path);
            rbRating = itemView.findViewById(R.id.rb_rating);
            tvOverview = itemView.findViewById(R.id.tv_overview);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        MovieData itemMovieData = movieResults.get(position);
                        Intent intent = new Intent(context, DetailMovieActivity.class);
                        intent.putExtra("movies", itemMovieData);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                    }
                }
            });
        }

    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        private LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }

}
