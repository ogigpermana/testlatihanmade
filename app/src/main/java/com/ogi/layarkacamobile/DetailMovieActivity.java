package com.ogi.layarkacamobile;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.ogi.layarkacamobile.model.movie.MovieData;
import com.squareup.picasso.Picasso;

public class DetailMovieActivity extends AppCompatActivity {

    TextView tvTitle, tvRating, tvOverview, tvRelease;
    ImageView ivPosterPath, ivBackdropPath;
    RatingBar rbRating;

    MovieData movie;
    int movie_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_movie);

        tvTitle = findViewById(R.id.tv_single_title);
        tvRating = findViewById(R.id.tv_single_rating);
        tvOverview = findViewById(R.id.tv_single_overview);
        tvRelease = findViewById(R.id.tv_single_release);
        ivPosterPath = findViewById(R.id.iv_poster_path);
        ivBackdropPath = findViewById(R.id.ivBackdrop);
        rbRating = findViewById(R.id.rb_single_rating);

        setupToolbar();

        Intent mySingleMovieIntent = getIntent();
        if (mySingleMovieIntent.hasExtra("movies")) {
            movie = getIntent().getParcelableExtra("movies");
            String IMAGE_BASE_URL_BACKDROP = "https://image.tmdb.org/t/p/w780";
            Picasso.get()
                    .load(IMAGE_BASE_URL_BACKDROP +movie.getBackdropPath())
                    .placeholder(R.drawable.default_backdrop)
                    .error(R.drawable.broken_image)
                    .into(ivBackdropPath);
            String IMAGE_BASE_URL_POSTER = "https://image.tmdb.org/t/p/w500";
            Picasso.get()
                    .load(IMAGE_BASE_URL_POSTER +movie.getPosterPath())
                    .placeholder(R.drawable.default_poster_path)
                    .error(R.drawable.broken_image)
                    .into(ivPosterPath);
            tvTitle.setText(movie.getTitle());
            tvRelease.setText(movie.getReleaseDate());
            tvRating.setText(String.valueOf(movie.getVoteAverage()));
            tvOverview.setText(movie.getOverview());
            rbRating.setRating((float) (movie.getVoteAverage() / 2));
            movie_id = movie.getId();
        }
    }

    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
