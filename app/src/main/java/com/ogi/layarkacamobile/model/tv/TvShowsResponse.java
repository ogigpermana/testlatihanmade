package com.ogi.layarkacamobile.model.tv;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TvShowsResponse implements Parcelable {
    @SerializedName("page")
    @Expose
    private Integer page;
    @SerializedName("total_results")
    @Expose
    private Integer totalResults;
    @SerializedName("total_pages")
    @Expose
    private Integer totalPages;
    @SerializedName("results")
    @Expose
    private List<TvShowData> results;

    public TvShowsResponse() {}

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(Integer totalResults) {
        this.totalResults = totalResults;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

    public List<TvShowData> getResults() {
        return results;
    }

    public void setResults(List<TvShowData> results) {
        this.results = results;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.page);
        dest.writeValue(this.totalResults);
        dest.writeValue(this.totalPages);
        dest.writeTypedList(this.results);
    }

    protected TvShowsResponse(Parcel in) {
        this.page = (Integer) in.readValue(Integer.class.getClassLoader());
        this.totalResults = (Integer) in.readValue(Integer.class.getClassLoader());
        this.totalPages = (Integer) in.readValue(Integer.class.getClassLoader());
        this.results = in.createTypedArrayList(TvShowData.CREATOR);
    }

    public static final Parcelable.Creator<TvShowsResponse> CREATOR = new Parcelable.Creator<TvShowsResponse>() {
        @Override
        public TvShowsResponse createFromParcel(Parcel source) {
            return new TvShowsResponse(source);
        }

        @Override
        public TvShowsResponse[] newArray(int size) {
            return new TvShowsResponse[size];
        }
    };
}
