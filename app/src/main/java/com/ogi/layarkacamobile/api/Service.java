package com.ogi.layarkacamobile.api;

import com.ogi.layarkacamobile.model.movie.MoviesResponse;
import com.ogi.layarkacamobile.model.tv.TvShowsResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Service {
    @GET("movie/popular")
    Call<MoviesResponse> getPopularMovies(
            @Query("api_key") String apiKey,
            @Query("page") int pageIndex
    );

    @GET("tv/popular")
    Call<TvShowsResponse> getPopularTvShows(
            @Query("api_key") String apiKey,
            @Query("page") int pageIndex
    );
}
